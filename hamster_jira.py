#!/usr/bin/env python

from jira import JIRA, JIRAError
import calendar
from datetime import date, datetime, timedelta
from dateutil.parser import parse as dateutil_parse
from dateutil.tz import *   # needed to fix time zone info of hamster time stamps.
import argparse
import getpass
import collections
import difflib
import sys
import inspect
import re
import logging
import os
import keyring

import datetime as pdt  # standard datetime

try:
	import ConfigParser
except ImportError:
	import configparser as ConfigParser
	pass

try:
    from hamster.configuration import runtime
except ImportError:
	from hamster.lib.configuration import runtime
	pass

logger = logging.getLogger(__name__)

WINDOWS_LINE_ENDING = '\r\n'
UNIX_LINE_ENDING = '\n'
MAC_LINE_ENDING = '\r'

def date_format():
	return "%Y-%m-%d"

def time_format():
	return "%H:%M"

def date2str(datetime_val):
	return datetime_val.strftime(date_format())

def time2str(datetime_val):
	return datetime_val.strftime(time_format())

def help_sample_config():
	sample_config_file='''
[login]
user=me
pwd=mine
keyring=false

[jira]
url=http://my.jira

[issue]
blacklist=SET-01,SOL-02

[proxy]
http=
https='''
	return sample_config_file

def help_epilog(config_default_path, help_sample_config):
	epilog='''
Press ENTER to skip an issue, or 'q' + ENTER to quit.

A config file is not required but might ease usage (default path '%s').
Some options like proxy settings and issue blacklist are only available via the config file.
Commandline options overwrite config file options.

Even if it is possible, storing passwords in the config file is not recommended!

The config file may look like:
%s
'''%(config_default_path, help_sample_config)
	return epilog

def load_config(config_path):
	config = ConfigParser.ConfigParser() if sys.version_info >= (3, 2) else ConfigParser.SafeConfigParser()
	realpath = os.path.realpath(os.path.expanduser(config_path))
	nread = config.read(realpath)
	if len(nread) < 1:
		raise Exception("Config file not readable '%s'"%(realpath))
	nested_dict = lambda: collections.defaultdict(nested_dict)
	cfg_dict = nested_dict()
	for section in config.sections():
		cfg_dict[section] = dict(config.items(section))
	if "issue" in cfg_dict and "blacklist" in cfg_dict["issue"]:
		cfg_dict["issue"]["blacklist"] = cfg_dict["issue"]["blacklist"].split()

	return cfg_dict

def user_input(desc, default="y"):
	user_input = input(desc) if sys.version_info >= (3, 2) else raw_input(desc)
	answer = user_input.strip()
	if answer == 'q':
		exit(0)
	elif answer == 'n':
		answer = "" # skip to next
	elif answer == "":
		answer = default
	return answer

class TodayAction(argparse.Action):
	def __call__(self, parser, namespace, values, option_string=None):
		setattr(namespace, 'start_date',  date.today())
		setattr(namespace, 'end_date',  date.today())

class YesterdayAction(argparse.Action):
	def __call__(self, parser, namespace, values, option_string=None):
		yesterday = date.today() - timedelta(days = 1)
		setattr(namespace, 'start_date',  yesterday)
		setattr(namespace, 'end_date',  yesterday)

def valid_date_type(arg_date_str):
	# taken from: monkut/argparse_date_datetime_custom_types.py (https://gist.github.com/monkut/e60eea811ef085a6540f)
	"""custom argparse *date* type for user dates values given from the command line"""
	try:
		return datetime.strptime(arg_date_str, date_format())
	except ValueError:
		raise argparse.ArgumentTypeError("Given Date ({0}) not valid! Expected format, YYYY-MM-DD!".format(arg_date_str))

def parse_args():
	config_default_path = '~/.hamster_jira.cfg'

	handler = logging.StreamHandler(sys.stdout)
	handler.setLevel(logging.DEBUG)
	formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
	handler.setFormatter(formatter)
	logger.addHandler(handler)

	parser = argparse.ArgumentParser(
		epilog=help_epilog(config_default_path, help_sample_config()),
		formatter_class=argparse.RawTextHelpFormatter
	)
	parser.add_argument('-d', '--dry-run', dest='dry_run', action='store_true')
	parser.add_argument('-c', '--config', dest='config_path', default=config_default_path)
	
	date_format_help = "format '%s'"%(date_format().replace('%', '%%'))

	parser.add_argument('-t', '--today', dest='today', action=TodayAction, nargs=0)
	parser.add_argument('-y', '--yesterday', dest='yesterday', action=YesterdayAction, nargs=0)
	parser.add_argument('-s', '--start', dest='start_date', type=valid_date_type, action='store', help=date_format_help)
	parser.add_argument('-e', '--end', dest='end_date', type=valid_date_type, action='store', help=date_format_help)
	parser.add_argument('-u', '--user', dest='user', type=str, help="Jira user name")
	parser.add_argument('-p', '--pwd', dest='pwd', type=str, help="Jira password for the given user")
	parser.add_argument('-k', '--keyring', dest='keyring', action='store_true', help="Use keyring for password")
	parser.add_argument('--pwdchange', dest='pwd_change', action='store_true', help="Change stored keyring passwword")
	parser.add_argument('-a', '--url', dest='url', type=str, help="Jira server URL/address")
	parser.add_argument('-l', '--log', dest='loglevel', type=str, default='INFO', help="Set log level. Default is INFO")

	args = parser.parse_args()

	logger.setLevel(args.loglevel.upper())

	if args.start_date is None:
		setattr(args, 'start_date',  date.today())
	if args.end_date is None:
		setattr(args, 'end_date',  date.today())
	return args

def extract_jira_issue_key(fact, jira, issue_key_blacklist):
	#issue_key_rgx = re.compile("[A-Z]{1,3}-[0-9]{1,}")

	issue_key_rgx = re.compile('([A-Za-z][A-Za-z0-9]+-[0-9]+)')

	fields = [fact.activity] + fact.tags
	logger.debug('Searching ticket in: %s', fields)

	existing_issues = []

	for field in fields:
		for possible_issue in issue_key_rgx.findall(field):
			logger.info('Lookup issue for activity "%s"', possible_issue)
			try:
				jira.issue(possible_issue)
				logger.info('Found existing issue "%s" in "%s"', possible_issue, field)
				if possible_issue in issue_key_blacklist:
					logger.warning('Issue "%s" ignored because it is blacklisted...', possible_issue)
				elif possible_issue in existing_issues:
					logger.debug('Issue "%s" already in list', possible_issue)
				else:
					existing_issues.append(possible_issue)
			except JIRAError as e:
				if e.text == 'Issue Does Not Exist':
					logger.warning('Tried issue "%s", but does not exist. ', possible_issue)
				else:
					logger.exception('Error communicating with Jira')

	if len(existing_issues) > 1:
		logger.warning("Multiple issues for fact: %s", existing_issues)

	issue_key = existing_issues[0] if len(existing_issues) else None

	return issue_key

def has_worklog_entry_with_starttime(worklogs, fact_started, user):

	wl_entries = []

	for wl_entry in worklogs:
		entry_started = dateutil_parse(wl_entry.started)
		#logger.debug('WL-ID %s, author "%s", start time "%s"', wl_entry.id, wl_entry.author.name, str(entry_started))
		if user == wl_entry.author.name and fact_started == entry_started:
			wl_entries.append(wl_entry)
			logger.debug('Matching WL-ID %s, author "%s", start time "%s"', wl_entry.id, wl_entry.author.name, str(entry_started))
			#logger.debug("Worklog: %r", inspect.getmembers(entry))

	return wl_entries

def export_facts_to_jira(jira, facts, config, args):
	fact_issue_matching = {}
	user = config["login"]["user"]

	for fact in facts:
		logger.info("----------------------")
		#logger.debug(inspect.getmembers(fact))

		if not fact.end_time:
			logger.warning("Fact without end_time: %s, %s -  %s "%(date2str(fact.start_time), time2str(fact.start_time), fact.activity))
			continue

		# round down to full minutes as it is done on jira bridge
		real_seconds = fact.delta.total_seconds()
		total_seconds = real_seconds - (real_seconds % 60)
			
		logger.info("%s, %s - %s (%d min): %s "%(date2str(fact.start_time), time2str(fact.start_time), time2str(fact.end_time), total_seconds/60,  fact.activity))
		issue_key = extract_jira_issue_key(fact, jira, config["issue"]["blacklist"])

		if not issue_key:
			logger.info("Skipping fact ...")
			continue

		issue = jira.issue(issue_key)
		worklog = jira.worklogs(issue_key)
		logger.info('Jira issue "%s", summary: %s', issue_key, issue.fields.summary)
		#logger.debug(inspect.getmembers(issue))

		htime = fact.start_time
		fact_started = pdt.datetime(year=htime.year, month=htime.month, day=htime.day,
									hour=htime.hour, minute=htime.minute, second=htime.second)


		comment = "" if not fact.description else str(fact.description)

		'''
		Hamster stores all times as local times and at least my tested versions of the hamster python lib don't add
		time zone info to the time stamps so we need to localize them.
		ATTENTION: If some of your worklog entries seem to be uploaded with the wrong time zone please also check
		           your python Jira client lib. There are versions with a really stupid bug out there sending all
		           time stamps with time zone "+0000". See also https://github.com/pycontribs/jira/issues/719
		'''
		if fact_started.tzinfo is None:
			logger.info("Start time without timezone. Use local timzone info!")
			fact_started = fact_started.replace(tzinfo=tzlocal())


		entries = has_worklog_entry_with_starttime(worklog, fact_started, user)
		if not entries:
			logger.info('"%s" - "%s" - Adding %d sec, start-time "%s"(%s), comment:\r\n%s',
						issue_key, issue.fields.summary, total_seconds, str(fact.start_time), str(fact_started), comment)
			if not args.dry_run:
				new_entry = jira.add_worklog(issue_key, timeSpentSeconds=total_seconds, comment=comment, started=fact_started)
				logger.debug('New worklog entry: %r', new_entry)
			else:
				logger.info("* dry run *")
		elif len(entries) > 1:
			logger.error('Multiple (%u) worklog entries issue "%s", user "%s" for start-time "%s"',
						 len(entries), issue_key, user, str(fact.start_time))
			logger.error("Skipping...")
		else:
			# now we only have a single existing worklog entry left
			entry = entries[0]
			update = False
			# fix comment line endings for comparision
			local_comment = comment.replace(WINDOWS_LINE_ENDING, UNIX_LINE_ENDING).replace(MAC_LINE_ENDING, UNIX_LINE_ENDING)
			remote_comment = entry.comment.replace(WINDOWS_LINE_ENDING, UNIX_LINE_ENDING).replace(MAC_LINE_ENDING, UNIX_LINE_ENDING)
			new_comment = local_comment
			new_duration = entry.timeSpentSeconds

			if total_seconds != entry.timeSpentSeconds:
				logger.warning('Updating worklog entry duration from %u to %u seconds?', entry.timeSpentSeconds, total_seconds)
				if user_input("Update worklog duration from %u to %u seconds (y)?" % (entry.timeSpentSeconds, total_seconds)).lower() == "y":
					update = True
					new_duration = total_seconds
			if local_comment != remote_comment:
				logger.debug('Local WL comment bytes:\r\n%r', local_comment)
				logger.debug('Remote WL comment bytes:\r\n%r', remote_comment)
				diff = difflib.ndiff(local_comment.splitlines(True), remote_comment.splitlines(True))
				logger.warning('Updating worklog comment? Diff:\r\n%s', ''.join(diff))
				if user_input("Update worklog comment (y)?").lower() == "y":
					update = True
					new_comment = local_comment

			if not update:
				logger.debug('Keep existing worklog entry!')
			elif args.dry_run:
				logger.debug('* dry run * - Keep existing worklog entry!')
			else:
				logger.info('"%s" - "%s" - Updating %d sec, start-time "%s"(%s), comment:\r\n%s',
							issue_key, issue.fields.summary, new_duration, str(fact.start_time), str(fact_started), new_comment)
				entry.delete()
				jira.add_worklog(issue_key, timeSpentSeconds=new_duration, comment=new_comment, started=fact_started)

		logger.info("----------------------\r\n\r\n")





if __name__ == '__main__':
	args = parse_args()
	config = load_config(args.config_path)

	# check possible command line options for Jira settings and overwrite config file settings
	if args.url: config["jira"]["url"] = args.url
	if args.user: config["login"]["user"] = args.user
	if args.pwd: config["login"]["pwd"] = args.pwd
	if args.keyring: config["login"]["keyring"] = args.keyring
	keyring_pwd = None

	proxy_settings = None if not "proxy" in config else config["proxy"]

	if not "pwd" in config["login"]:
		logger.debug("No password from config or cmdline!")
		config["login"]["pwd"] = None

	if not "keyring" in config["login"]:
		logger.debug("No config for keyring usage!")
		config["login"]["keyring"] = False

	if config["login"]["keyring"]:
		logger.debug("Used keyring: %s"%keyring.get_keyring())
		keyring_pwd = config["login"]["pwd"] = keyring.get_password("hamster-jira", config["login"]["user"])

	logger.debug("User: %s, Pwd: %s, Use-Keyring: %s."%(config["login"]["user"], config["login"]["pwd"], config["login"]["keyring"]))

	if config["login"]["pwd"] == None or args.pwd_change:
		config["login"]["pwd"] = getpass.getpass(prompt="Jira password: ")
		logger.debug("New pwd: %s for user %s, Use-Keyring: %s."%(config["login"]["pwd"], config["login"]["user"], config["login"]["keyring"]))

	if config["login"]["keyring"] and config["login"]["pwd"] != keyring_pwd:
		keyring.set_password("hamster-jira", config["login"]["user"], config["login"]["pwd"])
		logger.debug("New keyring pwd: %s for user %s."%(config["login"]["pwd"], config["login"]["user"]))


	jira = JIRA(config["jira"]["url"], proxies=proxy_settings, basic_auth=(config["login"]["user"], config["login"]["pwd"]) )
	facts = runtime.storage.get_facts(args.start_date, args.end_date)

	if not facts:
		logger.error("No hamster facts found for the given time range (%s - %s)."%(date2str(args.start_date), date2str(args.end_date)))
	else:
		export_facts_to_jira(jira, facts, config, args)
